package org.openrdf.repository.object;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.openrdf.annotations.Iri;
import org.openrdf.repository.object.base.ObjectRepositoryTestCase;

import junit.framework.Test;

public class AbstractConceptTest extends ObjectRepositoryTestCase {

	public static Test suite() throws Exception {
		return ObjectRepositoryTestCase.suite(AbstractConceptTest.class);
	}

	public static abstract class Person implements RDFObject {
		@Iri("urn:test:name")
		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public abstract String getFirstName();
	}

	public static abstract class FirstNameSupport {
		public abstract String getName();
		public String getFirstName() {
			return getName().split(" ")[0];
		}
	}

	@Override
	public void setUp() throws Exception {
		config.addConcept(Person.class, SimpleValueFactory.getInstance().createIRI("urn:test:Person"));
		config.addBehaviour(FirstNameSupport.class, SimpleValueFactory.getInstance().createIRI("urn:test:Person"));
		super.setUp();
	}

	public void testAbstractConcept() throws Exception {
		IRI id = vf.createIRI("urn:test:me");
		Person me = con.addDesignation(con.getObject(id), Person.class);
		me.setName("James Leigh");
		assertEquals("James", me.getFirstName());
	}
}
