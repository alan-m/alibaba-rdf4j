package org.openrdf.repository.object;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.Repository;
import org.openrdf.repository.object.base.RepositoryTestCase;
import org.openrdf.repository.object.concepts.Person;
import org.openrdf.repository.object.config.ObjectRepositoryFactory;

import junit.framework.Test;

public class AddTypeTest extends RepositoryTestCase {

	public static Test suite() throws Exception {
		return RepositoryTestCase.suite(AddTypeTest.class);
	}

	private ObjectRepository factory;

	private ObjectConnection manager;

	private ObjectConnection conn;

	private ValueFactory vf;

	public void testCreateBean() throws Exception {
		assertEquals(0, conn.size());
		manager.addDesignation(manager.getObjectFactory().createObject(), Person.class);
		assertEquals(1, conn.size());
		assertTrue(conn.hasStatement((IRI) null, RDF.TYPE, vf
				.createIRI("urn:foaf:Person")));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		factory = (ObjectRepository) repository;
		manager = factory.getConnection();
		conn = manager;
		vf = conn.getValueFactory();
	}

	@Override
	protected Repository getRepository() throws Exception {
		return new ObjectRepositoryFactory().createRepository(super.getRepository());
	}

	@Override
	protected void tearDown() throws Exception {
		manager.close();
		super.tearDown();
	}
}
