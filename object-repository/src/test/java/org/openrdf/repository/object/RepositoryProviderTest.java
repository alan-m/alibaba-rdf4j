package org.openrdf.repository.object;

import java.io.File;

import org.eclipse.rdf4j.common.io.FileUtil;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryProvider;

import junit.framework.TestCase;

public class RepositoryProviderTest extends TestCase {

	public void testFileURI() throws Exception {
		File dir = FileUtil.createTempDir(RepositoryProviderTest.class.getSimpleName());
		try {
			RepositoryManager byfile = RepositoryProvider.getRepositoryManager(dir);
			RepositoryManager byurl = RepositoryProvider.getRepositoryManager(dir.toURI().getRawPath());
			try {
				assertEquals(byfile, byurl);
			} finally {
				byfile.shutDown();
				byurl.shutDown();
			}
		} finally {
			FileUtil.deleteDir(dir);
		}
	}
}
