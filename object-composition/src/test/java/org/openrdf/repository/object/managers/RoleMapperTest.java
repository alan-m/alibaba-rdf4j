package org.openrdf.repository.object.managers;

import java.util.Collection;

import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.openrdf.annotations.Mixin;

import junit.framework.TestCase;

public class RoleMapperTest extends TestCase {

	public abstract class BehaviourClass {
		public boolean isBehaviourClass() {
			return true;
		}
	}

	@Mixin(BehaviourClass.class)
	public interface MixedClass {
		
	}

	@Mixin(name="org.openrdf.repository.object.managers.RoleMapperTest$BehaviourClass")
	public interface MixedClassName {
		
	}

	public void testMixinClass() throws Exception {
		RoleMapper rm = new RoleMapper();
		ValueFactory vf = SimpleValueFactory.getInstance();
		rm.addConcept(MixedClass.class, vf.createIRI("urn:MixedClass"));
		Collection<Class<?>> roles = rm.findRoles(vf.createIRI("urn:MixedClass"));
		assertTrue(roles.contains(BehaviourClass.class));
	}

	public void testMixinName() throws Exception {
		RoleMapper rm = new RoleMapper();
		ValueFactory vf = SimpleValueFactory.getInstance();
		rm.addConcept(MixedClassName.class, vf.createIRI("urn:MixedClass"));
		Collection<Class<?>> roles = rm.findRoles(vf.createIRI("urn:MixedClass"));
		assertTrue(roles.contains(BehaviourClass.class));
	}

}
