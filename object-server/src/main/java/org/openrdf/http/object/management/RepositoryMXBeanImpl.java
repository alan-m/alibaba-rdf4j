/*
 * Copyright (c) 2014 3 Round Stones Inc., Some Rights Reserved
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.openrdf.http.object.management;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;

import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.query.BooleanQuery;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.Query;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.resultio.text.tsv.SPARQLResultsTSVWriter;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.rio.ParserConfig;
import org.eclipse.rdf4j.rio.helpers.BasicParserSettings;
import org.eclipse.rdf4j.rio.turtle.TurtleWriter;
import org.openrdf.http.object.io.ArrangedWriter;
import org.openrdf.repository.object.ObjectConnection;
import org.openrdf.repository.object.ObjectRepository;
import org.openrdf.repository.object.exceptions.ObjectStoreConfigException;
import org.openrdf.store.blob.BlobObject;
import org.openrdf.store.blob.BlobStore;
import org.slf4j.LoggerFactory;

public class RepositoryMXBeanImpl implements RepositoryMXBean {

	private final org.slf4j.Logger logger = LoggerFactory.getLogger(RepositoryMXBeanImpl.class);
	private final ParserConfig parserConfig = new ParserConfig();
	private final ObjectRepositoryManager manager;
	private final String id;

	public RepositoryMXBeanImpl(ObjectRepositoryManager manager, String id) {
		this.manager = manager;
		this.id = id;
		parserConfig.set(BasicParserSettings.PRESERVE_BNODE_IDS, true);
	}

	public int getMaxQueryTime() throws RDF4JException {
		return getObjectRepository().getMaxQueryTime();
	}

	public void setMaxQueryTime(int maxQueryTime) throws RDF4JException {
		getObjectRepository().setMaxQueryTime(maxQueryTime);
	}

	public boolean isIncludeInferred() throws RDF4JException {
		return getObjectRepository().isIncludeInferred();
	}

	public void setIncludeInferred(boolean includeInferred) throws RDF4JException {
		getObjectRepository().setIncludeInferred(includeInferred);
	}

	public BlobStore getBlobStore() throws RDF4JException, ObjectStoreConfigException {
		return getObjectRepository().getBlobStore();
	}

	public void setBlobStore(BlobStore store) throws RDF4JException {
		getObjectRepository().setBlobStore(store);
	}

	public String[] sparqlQuery(String query) throws RDF4JException, IOException {
		RepositoryConnection conn = this.getConnection();
		try {
			Query qry = conn.prepareQuery(QueryLanguage.SPARQL, query);
			if (qry instanceof TupleQuery) {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				SPARQLResultsTSVWriter writer = new SPARQLResultsTSVWriter(out);
				((TupleQuery) qry).evaluate(writer);
				return new String(out.toByteArray(), "UTF-8").split("\r?\n");
			} else if (qry instanceof BooleanQuery) {
				return new String[]{String.valueOf(((BooleanQuery) qry).evaluate())};
			} else if (qry instanceof GraphQuery) {
				StringWriter string = new StringWriter(65536);
				TurtleWriter writer = new TurtleWriter(string);
				((GraphQuery) qry).evaluate(new ArrangedWriter(writer));
				return string.toString().split("(?<=\\.)\r?\n");
			} else {
				throw new RepositoryException("Unknown query type: " + qry.getClass().getSimpleName());
			}
		} finally {
			conn.close();
		}
	}

	public void sparqlUpdate(String update) throws RDF4JException, IOException {
		RepositoryConnection conn = this.getConnection();
		try {
			logger.info(update);
			conn.prepareUpdate(QueryLanguage.SPARQL, update).execute();
		} finally {
			conn.close();
		}
	}

	public String readCharacterBlob(String uri) throws RDF4JException, IOException {
		ObjectConnection conn = getObjectConnection();
		try {
			return conn.getBlobObject(uri).getCharContent(true).toString();
		} finally {
			conn.close();
		}
	}

	public byte[] readBinaryBlob(String uri) throws RDF4JException, IOException {
		ObjectConnection conn = getObjectConnection();
		try {
			BlobObject blob = conn.getBlobObject(uri);
			InputStream in = blob.openInputStream();
			try {
				ByteArrayOutputStream baos = new ByteArrayOutputStream((int) blob.getLength());
				byte[] buf = new byte[1024];
				int read;
				while ((read = in.read(buf)) >= 0) {
					baos.write(buf, 0, read);
				}
				return baos.toByteArray();
			} finally {
				in.close();
			}
		} finally {
			conn.close();
		}
	}

	public void storeCharacterBlob(String uri, String content) throws RDF4JException, IOException {
		ObjectConnection conn = getObjectConnection();
		try {
			logger.warn("Replacing {}", uri);
			Writer writer = conn.getBlobObject(uri).openWriter();
			try {
				writer.write(content);
			} finally {
				writer.close();
			}
		} finally {
			conn.close();
		}
	}

	public void storeBinaryBlob(String uri, byte[] content) throws RDF4JException, IOException {
		ObjectConnection conn = getObjectConnection();
		try {
			logger.warn("Replacing {}", uri);
			OutputStream out = conn.getBlobObject(uri).openOutputStream();
			try {
				out.write(content);
			} finally {
				out.close();
			}
		} finally {
			conn.close();
		}
	}

	private RepositoryConnection getConnection() throws RDF4JException {
		RepositoryConnection con = getRepository().getConnection();
		con.setParserConfig(parserConfig);
		return con;
	}

	private ObjectConnection getObjectConnection() throws RDF4JException {
		ObjectConnection con = getObjectRepository().getConnection();
		con.setParserConfig(parserConfig);
		return con;
	}

	private ObjectRepository getObjectRepository() throws RDF4JException {
		return manager.getObjectRepository(id);
	}

	private Repository getRepository() throws RDF4JException {
		return manager.getRepository(id);
	}

}
