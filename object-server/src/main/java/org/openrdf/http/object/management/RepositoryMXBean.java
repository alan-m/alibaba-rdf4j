/*
 * Copyright (c) 2014 3 Round Stones Inc., Some Rights Reserved
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.openrdf.http.object.management;

import java.io.IOException;

import org.eclipse.rdf4j.RDF4JException;

public interface RepositoryMXBean {

	int getMaxQueryTime() throws RDF4JException;

	void setMaxQueryTime(int maxQueryTime) throws RDF4JException;

	boolean isIncludeInferred() throws RDF4JException;

	void setIncludeInferred(boolean includeInferred) throws RDF4JException;

	String[] sparqlQuery(String query) throws RDF4JException, IOException;

	void sparqlUpdate(String update) throws RDF4JException, IOException;

	String readCharacterBlob(String uri) throws RDF4JException, IOException;

	byte[] readBinaryBlob(String uri) throws RDF4JException, IOException;

	void storeCharacterBlob(String uri, String content) throws RDF4JException, IOException;

	void storeBinaryBlob(String uri, byte[] content) throws RDF4JException, IOException;

}

